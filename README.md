# terraform-aws-module-vpc-route-table
* VPC에 라우팅 테이블과 룰을 셋업 하는 공통 모듈
* 하나의 라우팅 테이블만 관리할 수 있음

## Usage

### `terraform.tfvars`
* 모든 변수는 적절하게 변경하여 사용
```
account_id = "123456789012" # 아이디 변경 필수
region     = "ap-northeast-2" # 리전 변경 필수

vpc_filter = {
  "Name" = "dev-common-vpc"
}

# 라우팅 테이블 이름은 서브넷 이름에 의해 자동 생성됨
# 예) dev-common-bastion-subnet-2a --> dev-common-bastion-route-table-2a 
# replace(each.value, "-subnet-", "-route-table-")
subnet_filter = {
  "Name" = "dev-common-bastion-subnet-2a"
}

internet_gateway_filter = {
  "Name" = "dev-common-vpc-igw"
}

#nat_gateway_filter = {
#  "Name" = "dev-common-vpc-natgw-2a"
#}

# 공통 tag, 생성되는 모든 리소스에 태깅
tags = {
  "CreatedByTerraform"     = "true"
  "TerraformModuleName"    = "terraform-aws-module-vpc-route-table"
  "TerraformModuleVersion" = "v1.0.0"
}
```
---

### `main.tf`
```
module "vpc-route-table" {
  source = "git::https://mz.gitlab.kis.finance/mzc/terraform-aws-module-vpc-route-table.git?ref=v1.0.0"

  account_id = var.account_id
  region     = var.region

  vpc_name    = var.vpc_filter["Name"]
  subnet_name = var.subnet_filter["Name"]

  vpc_id     = data.aws_vpc.this.id
  subnet_ids = data.aws_subnet_ids.this.ids

  current_id     = data.aws_caller_identity.current.account_id
  current_region = data.aws_region.current.name

  routing_rules = local.routing_rules
  
  tags = var.tags
}
```
---

### `provider.tf`
```
provider "aws" {
  region = var.region
}
```
---

### `terraform.tf`
```
terraform {
  required_version = ">= 1.1.2"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.71"
    }
  }

  # bucket과 key 생성 규칙
  # bucket = kis-{mts, mps}-{dev, test, stage, prod}-tf-state-backend
  # key    = ${ACCOUNT_ID}/{dev, test, stage, prod}/${VPC_LINEUP}/${AWS_SERVICE_NAME}/terraform.state
  # 여기서 ACCOUNT_ID는 2가지 경우가 있음
  # 1. RoleSwitch가 되어 실제 리소스가 배포되는 Account
  # 2. Mgmt 에서 자기 자신에서 배포하는 Account(예: kis-mgmt-prod에 생성되는 TGW)
  backend "s3" {
    bucket         = "kis-mts-dev-tf-state-backend"
    key            = "012345678912/dev/common/route-table/2a/bastion/terraform.state"
    region         = "ap-northeast-2"
    dynamodb_table = "terraform-state-locks"
    encrypt        = true
  }
}
```
---

### `locals.tf`
```
# 현재 지원되는 라우팅 테이블 target type
# internet gateway    : igw-
# nat gateway         : nat-
# vpc endpoind        : vpce-
# trangit gateway     : tgw-
# managed prefix list : pl-
locals {
  routing_rules = {
    igw = {
      dst_cidr    = "0.0.0.0/0"
      target_id   = "${data.aws_internet_gateway.this.id}"
    },
    nat = {
      dst_cidr    = "0.0.0.0/0"
      target_id   = "${data.aws_nat_gateway.this.id}"
    }
  }
}
```

### `data.tf`
```
data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "aws_vpc" "this" {
  dynamic "filter" {
    for_each = var.vpc_filter # block를 생성할 정보를 담은 collection 전달, 전달 받은 수 만큼 block 생성
    iterator = tag # 각각의 item 에 접근할 수 있는 라벨링 부여, content block에서 tag 를 사용하여 접근   
    content { # block안에서 실제 전달되는 값들
      name   = "tag:${tag.key}"
      values = [tag.value] # "Name" 값이 string일 경우 [] 사용
    }
  }
}

data "aws_subnet_ids" "this" {
  vpc_id = data.aws_vpc.this.id 
  dynamic "filter" {
    for_each = var.subnet_filter
    iterator = tag
    content {
      name   = "tag:${tag.key}"
      values = [tag.value]
    }
  }
}

data "aws_internet_gateway" "this" {
  dynamic "filter" {
    for_each = var.internet_gateway_filter
    iterator = tag
    content {
      name   = "tag:${tag.key}"
      values = [tag.value]
    }
  }
}

#data "aws_nat_gateway" "this" {
#  dynamic "filter" {
#    for_each = var.nat_gateway_filter
#    iterator = tag
#    content {
#      name   = "tag:${tag.key}"
#      values = [tag.value]
#    }
#  }
#}
```
---

### `variables.tf`
```
variable "account_id" {
  description = "List of Allowed AWS account IDs"
  type        = string
}

variable "region" {
  description = "AWS Region"
  type        = string
}

variable "vpc_filter" {
  description = "Filters to select subnets"
  type        = map(string)
}

variable "subnet_filter" {
  description = "A list of subnet ID to associate with."
  type        = map(string)
}

variable "internet_gateway_filter" {
  description = "A list of internet gateway ID to associate with."
  type        = map(string)
}

#variable "nat_gateway_filter" {
#  description = "A list of nat gateway ID to associate with."
#  type        = map(string)
#}

variable "tags" {
  description = "tag map"
  type        = map(string)
}
```
---

### `outputs.tf`
```
output "vpc-route-table" {
  description = "AWS VPC route table"
  value       = module.vpc-route-table
}
```

## 실행방법
```
terraform init -get=true -upgrade -reconfigure
terraform validate (option)
terraform plan -var-file=terraform.tfvars -refresh=false -out=planfile
terraform apply planfile
```
* "Objects have changed outside of Terraform" 때문에 `-refresh=false`를 사용
* 실제 UI에서 리소스 변경이 없어보이는 것과 low-level Terraform에서 Object 변경을 감지하는 것에 차이가 있는 것 같음, 다음 링크 참고
    * https://github.com/hashicorp/terraform/issues/28776
* 위 이슈로 변경을 감지하고 리소스를 삭제하는 케이스가 발생 할 수 있음