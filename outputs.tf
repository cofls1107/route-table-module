output "account_id" {
  description = "AWS Account ID"
  value       = var.account_id
}

output "current_id" {
  description = "Your current AWS Account ID"
  value       = var.current_id
}

output "region" {
  description = "AWS region"
  value       = var.region
}

output "current_region" {
  description = "Your current AWS region"
  value       = var.current_region
}

output "vpc_id" {
  description = "VPC ID"
  value       = var.vpc_id
}

output "vpc_name" {
  description = "VPC name"
  value       = var.vpc_name
}

output "subnet_ids" {
  description = "Subnet IDs"
  value       = var.subnet_ids
}

output "subnet_name" {
  description = "Subnet name"
  value       = var.subnet_name
}

output "routing_rules" {
  description = "rules for route table"
  value       = var.routing_rules
}