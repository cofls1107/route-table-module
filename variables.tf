variable "account_id" {
  description = "List of Allowed AWS account IDs"
  type        = string
}

variable "current_id" {
  description = "current id"
  type        = string
}

variable "region" {
  description = "AWS Region"
  type        = string
}

variable "current_region" {
  description = "AWS Region"
  type        = string
}

variable "vpc_name" {
  description = "VPC name tag"
  type        = string
}

variable "subnet_name" {
  description = "Subnet name for EKS node group"
  type        = string
}

variable "vpc_id" {
  description = "VPC ID"
  type        = string
}

variable "subnet_ids" {
  description = "Subnet IDs for EKS node group"
  type        = list(string)
}

variable "routing_rules" {
  description = "rules for route table"
  type        = map(any)
}

variable "tags" {
  description = "tag map"
  type        = map(string)
}