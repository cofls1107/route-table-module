resource "null_resource" "validate_account" {
  count = var.current_id == var.account_id ? 0 : "Please check that you are using the AWS account"
}

resource "null_resource" "validate_module_name" {
  count = local.module_name == var.tags["TerraformModuleName"] ? 0 : "Please check that you are using the Terraform module"
}

resource "null_resource" "validate_module_version" {
  count = local.module_version == var.tags["TerraformModuleVersion"] ? 0 : "Please check that you are using the Terraform module"
}

resource "aws_route_table" "this" {
  vpc_id = var.vpc_id

  dynamic "route" {
    for_each = var.routing_rules
    iterator = rt
    content {
      cidr_block                 = rt.value["dst_cidr"]
      gateway_id                 = substr(rt.value["target_id"], 0, 4) == "igw-"  ? rt.value["target_id"] : null
      nat_gateway_id             = substr(rt.value["target_id"], 0, 4) == "nat-"  ? rt.value["target_id"] : null
      vpc_endpoint_id            = substr(rt.value["target_id"], 0, 5) == "vpce-" ? rt.value["target_id"] : null
      transit_gateway_id         = substr(rt.value["target_id"], 0, 4) == "tgw-"  ? rt.value["target_id"] : null
      destination_prefix_list_id = substr(rt.value["target_id"], 0, 3) == "pl-"   ? rt.value["target_id"] : null
    }
  }

  tags = merge(var.tags, tomap({"Name" = format("%s", replace(var.subnet_name, "-subnet-", "-route-table-"))}))

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route_table_association" "this" {
  subnet_id      = var.subnet_ids[0]
  route_table_id = aws_route_table.this.id

  depends_on = [
    aws_route_table.this
  ]
}